sh namespace.sh
echo "##########################  deploy MYSQL : START #############################"
cd mysql && sh ./run.sh && cd ..
echo "##########################  deploy MYSQL : END #############################"



echo "##########################  deploy PHPMYADMIN : START #############################"
cd phpmyadmin && sh ./run.sh && cd ..
echo "##########################  deploy PHPMYADMIN : END #############################"



echo "##########################  deploy api-nestjs : START #############################"
cd api-nestjs && sh ./run.sh && cd ..
echo "##########################  deploy api-nestjs : END #############################"



echo "##########################  deploy WEB DASHBOARD : START #############################"
cd web-dashboard && sh ./run.sh && cd ..
echo "##########################  deploy WEB DASHBOARD : END #############################"


echo "##########################  deploy PORTAL DASHBOARD : START #############################"
cd web-portal && sh ./run.sh && cd ..
echo "##########################  deploy PORTAL DASHBOARD : END #############################"
