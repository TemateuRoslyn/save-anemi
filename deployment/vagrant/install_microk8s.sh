if ! command -v microk8s >/dev/null 2>&1; then
  echo "Microk8s is not installed. Installing..."
  sudo apt-get update
  sudo snap install microk8s --classic
else
  echo "Microk8s already installed. Skipping installation."
fi

# Install Docker (assuming it's not already installed)
if ! command -v docker >/dev/null 2>&1; then
  echo "Installing Docker..."
  snap install docker
else
  echo "Docker already installed. Skipping installation."
fi


# Wait for microk8s to be ready (if installed)
if command -v microk8s >/dev/null 2>&1; then
  microk8s.status --wait-ready
fi

# Enable microk8s add-ons (if installed)
if command -v microk8s >/dev/null 2>&1; then
  microk8s enable hostpath-storage
  microk8s enable ingress
  microk8s enable helm3
  microk8s enable dashboard
  microk8s enable dns
  microk8s enable gpu  # Enable with caution, consider security implications
  microk8s enable host-access  # Enable with caution, consider security implications
  microk8s enable rbac
  microk8s enable rook-ceph  # Consider if you need persistent storage with Ceph
  microk8s enable storage  # May be redundant with rook-ceph
  # Optional add-ons (uncomment if needed)
  # microk8s enable cert-manager
  # microk8s enable community
  # microk8s enable metrics-server
fi

if command -v microk8s >/dev/null 2>&1; then

    # Add user to microk8s group (assuming microk8s is installed)
    sudo usermod -a -G microk8s vagrant

    # Set kubectl alias (modify paths if needed)
    echo "alias kubectl='microk8s kubectl'" > /vagrant/.bash_aliases
    chown vagrant:vagrant /vagrant/.bash_aliases
    echo "alias kubectl='microk8s kubectl'" > /root/.bash_aliases
    chown root:root /root/.bash_aliases

    # Ensure vagrant user has permissions (assuming microk8s is installed)
    sudo chown -f -R vagrant:vagrant /vagrant/.kube

    # Get pod information and cluster status (if microk8s is installed)
    kubectl get pods --all-namespaces
    microk8s status

    microk8s start
fi

