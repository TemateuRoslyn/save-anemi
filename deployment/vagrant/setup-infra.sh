#!/bin/bash

echo "###################################   Installation de l'infrastructure   ####################################"


dossier_cible="anemi-vagrant-setup-infra"

# Vérifier si Git est installé
if ! command -v git >/dev/null;  then
    apt -y install git
fi
# Vérifier si le dossier d'initialisatoin de l'infra est present
if ! [ -d "$dossier_cible" ]; then
  git clone https://gitlab.com/TemateuRoslyn/anemi-vagrant-setup-infra.git
fi

cd "$dossier_cible"

git pull origin main && cd scipts

sudo sh mysql.sh
sudo sh apache2.sh
# sudo sh phpmyadmin.sh
sudo sh nodejs.sh
sudo sh java.sh

cd /vagrant/anemi.printer.4.0

echo "###################################   Demarrage des application de l'infrastructure   ####################################"


# cd src/frontend/web/react && npm run dev