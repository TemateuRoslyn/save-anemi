# Anemi Printer Dashboard - React Application

## Description

Il s'agit de l'application Web pour les administrateurs de la plateforme Anemi printer.
Cette version servira principalement au monitoring des ressources d'Anemi printer.

## Démarrage

Ce projet utilise Vite pour le développement et la construction des pages Web et Tailwind CSS pour le design des interfaces.

### Prérequis

* node@18.19.0 installé sur votre système.
* npm@10.2.3 installé sur votre système.

### Installation

1. Accédez au répertoire du projet.

```sh
cd src/frontend/web/react-admin 
```

2. Installez les dépendances :

```sh
npm install
```

4. Démarrer le projet

```sh
npm run dev
```

5. Visualiser l'application dashboard sur le port indiqué dans la console.