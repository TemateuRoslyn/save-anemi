# Anemi Printer Web Client - React Application

## Description

Il s'agit de la version Web de l'application Anemi-Printer 4.0. 
Cette version est destinée aux utilisateurs travaillant depuis des ordinateurs (connectés à internet) et souhaitant réaliser une impression dans l'environnement Anemi.

## Démarrage

Ce projet utilise Vite pour le développement et la construction des pages Web, ainsi que Tailwind CSS pour le design des interfaces.

### Prérequis

* node@18.19.0 installé sur votre système.
* npm@10.2.3 installé sur votre système.

### Installation

1. Accédez au répertoire du projet.

```sh
cd src/frontend/web/react 
```

2. Installez les dépendances :

```sh
npm install
```

4. Démarrer le projet

```sh
npm run dev
```

5. Visualiser l'application Web sur le port indiqué dans la console.