# Anemi Printer Mobile Client - React Native Application

## Description

Anemi Printer 4.0 est une application mobile conçue pour faciliter l'impression de documents directement depuis votre appareil mobile. Avec une interface conviviale et une intégration transparente avec les imprimantes, AnemiPrinter vise à simplifier le processus d'impression pour les utilisateurs en déplacement.

## Démarrage

Ce projet utilise Vite pour le développement et la construction ainsi que Tailwind CSS pour le design des interfaces.

### Prérequis

* node@18.19.0
* npm@10.2.3
* java 17 - openjdk 17.0.9 2023-10-17
* Android SDK Tools
* Android Studio latest
* Installer une machine virtuelle dans android Studio


### Installation & Démarrage

1. Accédez au répertoire du projet.

```sh
cd src/frontend/mobile/react-native 
```

2. Installez les dépendances :

```sh
npm install
```

4. Démarrez le serveur JS

```sh
npm run start
```
5. Démarrez l'émulateur ou connectez votre téléphone au PC en mode débogage. Le téléphone virtuel ou physique doit être démarré avant l'étape suivante

6. Lancez la génération de la l'APK et son l'installation dans le téléphone virtuel ou physique connecté.

```sh
npm run android
```
6. Visualiser l'application mobile dans le smartphone ou dans la téléphone virtuel.

### Génération de l'APK

Ici, vous pouvez générer directement l'application mobile sans avoir à démarrer la machine virtuelle ou même à connecter un smartphone.

En somme, après la génération, l'APK obtenue peut être transférée à une personne tierce pour d'éventuels tests.

1. Generer l'APK:

```sh
cd src/frontend/mobile/react-native 
npm run android-app-debug
```

2. Une fois l'application mobile générée, vous pouvez récupérer votre package à cet emplacement :

```sh
cd src/frontend/mobile/react-native/android/app/build/outputs/apk/debug
```
Le package se nomme: `app-debug.apk`