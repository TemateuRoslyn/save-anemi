import React from 'react';
import NavigationProvider from './src/navigation';

const App = () => {
  return (
      <NavigationProvider/>
  );
};

export default App;
