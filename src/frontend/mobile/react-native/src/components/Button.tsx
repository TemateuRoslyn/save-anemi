

import React, { memo } from 'react';
import { StyleSheet } from 'react-native';
import { Button as PaperButton, useTheme } from 'react-native-paper';
// import { theme } from '../core/theme';

type Props = React.ComponentProps<typeof PaperButton>;

const Button = ({ mode, style, children, ...props }: Props) => {
  const theme = useTheme();
  return(
  
    <PaperButton
      style={[
        styles.button,
        mode === 'outlined' ?  { backgroundColor: theme.colors.surface } : { backgroundColor: 'black' },
  
        style,
      ]}
      labelStyle={mode === 'outlined' ? styles.textOutline :  styles.text}
      mode={mode}
      {...props}
    >
      {children}
    </PaperButton>
  );
}



const styles = StyleSheet.create({
  button: {
    width: '100%',
    marginVertical: 10,
  },
  text: {
    fontWeight: 'bold',
    fontSize: 15,
    lineHeight: 26,
    color: '#fff',
  },
  textOutline: {
    fontWeight: 'bold',
    fontSize: 15,
    lineHeight: 26,
    color: 'black',
  },
});

export default memo(Button);
