import React, { memo } from 'react';
import { Image, StyleSheet } from 'react-native';

const LogoSecond = () => (
  <Image source={require('./../assets/logos/logo-anemi.png')} style={styles.image} />
);

const styles = StyleSheet.create({
  image: {
    width:400,
    height:400,
    marginBottom: 12,
  },
});

export default memo(LogoSecond);
