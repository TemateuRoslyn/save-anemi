// user.ts

// Définition du modèle User
export interface User {
    username: string;
    password: string;
    isAuthenticate?: boolean;
    isFirstLogin?: boolean;
};

export interface AuthResponse {
    status: string;
    message: string;
    user?: User;
    isAuthenticate?: boolean;
};

export interface ReduxActionType { 
    type: string, 
    payload: any 
}

  