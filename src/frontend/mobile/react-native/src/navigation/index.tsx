import React, { useEffect, useState } from "react";
import "react-native-gesture-handler";
// theme
import { 
  NavigationContainer,
  DarkTheme as NavigationDarkTheme,
  DefaultTheme as NavigationDefaultTheme,
} from "@react-navigation/native";
import {
  MD3DarkTheme,
  MD3LightTheme,
  adaptNavigationTheme,
} from 'react-native-paper';
import merge from 'deepmerge';
// redux
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useDispatch, useSelector } from "react-redux";
import { loginAction } from "../redux/actions/authAction";
import { Dispatch } from "redux";
import { AUTH_USER_ASC } from "../utils/constants";
// navigators
import AuthStackNavigator from "./stacks/AuthStackNavigator";
import BottomTabNavigator from "./bottom";


const NavigationProvider: React.FC = () => {
  const dispatch: Dispatch<any> = useDispatch();
  const auth = useSelector((state: any) => state.auth);
  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);

  useEffect(() => {
    const checkAuthentication = async () => {
      try {
        const userString = await AsyncStorage.getItem(AUTH_USER_ASC);
        if (userString !== null) {
          const user = JSON.parse(userString);
          console.log(`From app: ${JSON.stringify(user)}`);
          dispatch(loginAction(user));
          setIsAuthenticated(user.isAuthenticated || false);
        } else {
          setIsAuthenticated(false);
        }
      } catch (error) {
        console.error("Error checking authentication:", error);
      }
    };

    checkAuthentication();
  }, [dispatch]);

  return (
    <>
      {auth.isLoggedIn === true ? (
        <BottomTabNavigator />
      ) : (
        <AuthStackNavigator />
      )}
    </>
  );
};

export default NavigationProvider;
