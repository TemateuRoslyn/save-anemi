import React from 'react';
import { createMaterialBottomTabNavigator } from 'react-native-paper/react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { HomeScreen } from '../../screens/app';
import SettingsScreen from '../../screens/app/Settings';
import AbousUsScreen from '../../screens/app/Abous-Us';
import { useTheme } from 'react-native-paper';
import { PreferencesContext } from '../../core/PreferencesContext';

const Tab = createMaterialBottomTabNavigator();

const BottomTabNavigator = () => {
  const { toggleTheme, isThemeDark } = React.useContext(PreferencesContext);

  const theme = useTheme();
  return (
    <Tab.Navigator 
        
        initialRouteName={'HomeScreen'}
        activeColor={isThemeDark? 'black' : '#A03EA9'}
        inactiveColor={isThemeDark? '#A03EA9' : '#fff'}
        // inactiveColor={isThemeDark? 'black' : '#fff'}
        barStyle={{ 
          backgroundColor: theme.colors.onSurface,
          borderTopLeftRadius: 30, 
          borderTopRightRadius: 30, 
          overflow: 'hidden', 
        }}
        shifting={true}
        sceneAnimationEnabled={true}
        sceneAnimationType={'shifting'}
        >
      <Tab.Screen 
        name="AbousUsScreen" 
        component={AbousUsScreen}
        options={{
        
            tabBarLabel: 'About Us',
            tabBarIcon: ({ color, focused, }) => (
              <Ionicons name={focused ? 'people' : 'people-outline'} color={color} size={26} />
            ),
          }}
        />
      <Tab.Screen 
        name="HomeScreen"
        component={HomeScreen}
        options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({ color, focused }) => (
              <Ionicons name={focused ? 'home' : 'home-outline'} color={color} size={26} />
            ),
          }} />
       <Tab.Screen 
        name="SettingsScreen"
        component={SettingsScreen}
        options={{
            tabBarLabel: 'Settings',
            tabBarIcon: ({ color, focused }) => (
              <Ionicons name={focused ? 'settings' : 'settings-outline'} color={color} size={26} />
            ),
          }} />
    </Tab.Navigator>
  );
}

export default BottomTabNavigator;