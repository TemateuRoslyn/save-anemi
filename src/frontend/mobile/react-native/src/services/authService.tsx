import AsyncStorage from "@react-native-async-storage/async-storage";
import { AuthResponse, User } from "../models/models";
import { LOGIN_FAIELD, LOGIN_SUCCESS, LOGOUT } from "../redux/actions/type";
import { AUTH_USER_ASC } from "../utils/constants";

/**
 * Fonction permettant de connecter un utilisateur.
 * @param user Les informations de l'utilisateur à connecter.
 * @returns Une promesse résolue avec un objet contenant le statut de la connexion et un message, ou undefined si la connexion échoue.
 */
const logIn =  (user: User): AuthResponse => {
  const { username, password } = user;
  console.log('logIn: ');
  console.log(user);

  AsyncStorage.setItem(AUTH_USER_ASC, JSON.stringify(user));
  console.log(`stock dans async storage: ${JSON.stringify(user)}`);

  if (username === "maestros@gmail.com" && password === "Admin123") {

    user.isAuthenticate = true;
    user.isFirstLogin = false;

    return {
      status: LOGIN_SUCCESS,
      message: "You are redirecting to home page",
      user: user,
      isAuthenticate: true
    };
  } else {
    
    user.isAuthenticate = false;
    return {
      status: LOGIN_FAIELD,
      message: "Bad credentials",
      isAuthenticate: false
    };
  }
};

/**
 * Fonction permettant de déconnecter l'utilisateur.
 * @returns Une promesse résolue avec un objet contenant le statut de la déconnexion et un message.
 */
const logOut = () => {
  AsyncStorage.clear();
  return {
    status: LOGOUT,
    message: "You are logged out",
  };
};

  
export default {
    logIn,
    logOut,
  };