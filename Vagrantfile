# Load YAML library for parsing settings.yml
require 'yaml'

# Load settings from settings.yml
settings = YAML.load_file('./deployment/vagrant/settings.yml')

# Define Vagrant.configure block to create VMs
Vagrant.configure("2") do |config|
  
  # Loop through each VM configuration
  settings['boxs'].each do |box|

    # Define VM configuration
    config.vm.define box['vm'] do |node|

      # Configure VM settings
      node.vm.box = box['box']
      node.vm.hostname = box['vm']
      node.vm.network box['network'], ip: box['ip']
      node.vm.provider box['provider'] do |vb|
        vb.memory = box['memory']
        vb.cpus = box['cpus']
      end

      # Configure forwarded ports
      box['ports'].each do |port|
        node.vm.network "forwarded_port", guest: port['guest'], host: port['host'], host_ip: port['host_ip'], guest_ip: port['guest_ip'], auto_correct: port['auto_correct']
      end

      # Provision VM with shell script
      box['provisioners'].each do |provisioner|
        node.vm.provision provisioner['type'], path: provisioner['path']
      end

      if box.key?('synced_folder')
        # Synced folders
        box['synced_folder'].each do |folder|
          node.vm.synced_folder folder['host_path'], folder['guest_path'], type: folder['type'], mount_options: folder['mount_options'], rsync__exclude: folder['rsync_exclude'], rsync__args: folder['rsync__args']
        end
      end
    end
  end
end
