# Anemi Printer 4.0

## Description

Ce projet est une plateforme qui permet de gérer les opérations d'impression sur plusieurs imprimantes.

## Fonctionnalités

* Interface visuelle pour l'impression de document.
* Graphiques et tableaux pour visualiser les données d'impression.
* Fonctionnalités de filtrage, de tri et de recherche.
* Possibilité de programmer des impressions.

## Microservice

Ce projet dispose d'une architecture en micro-service composee de plusieurs peties appliactions.

# L'appliaction backend Nest.js

* le backend  utilisé ici est  Nest.js.
* Cette applicatoin est disponible ici:
```sh
cd src/backend/nestjs
```
* Les détails concernant la documentation de cette application backend (la version, l'installation de dépendances, démarrage...) sont precisées dans 
cd fichier README.md.
```sh
cat src/backend/nestjs/README.md
```


# L'appliaction mobile React Native

* Le Frontend Mobile `src/frontend/mobile` : 
* Il s'agit d'une application react-native disponible ici:
```sh
cd src/frontend/mobile/react-native
```
* Les détails concernant la documentation de cette application mobile (la version, l'installation de dépendances, démarrage...) sont precisées dans 
cd fichier README.md.
```sh
cat src/frontend/mobile/react-native/README.md
```


# Le Web site client React.js pour les impression.

Il s'agit de l'application web ou les utilisateur realisent des impression de leur documents.

* Le Web site client `src/frontend/web` : 
* Cette application react.js est disponible ici:
```sh
cd src/frontend/web/react
```
* Les détails concernant la documentation de cette application mobile (la version, l'installation de dépendances, démarrage...) sont precisées dans 
cd fichier README.md.
```sh
cat src/frontend/web/react/README.md
```

# Le Web site dashboard React.js pour l'administration du systeme.

Il s'agit de l'application web ou les administrateur monitore Animi printer.

* Le Web site dashboard `src/frontend/web` : 
* Cette application react.js est disponible ici:
```sh
cd src/frontend/web/react-admin
```
* Les détails concernant la documentation de cette application mobile (la version, l'installation de dépendances, démarrage...) sont precisées dans 
cd fichier README.md.
```sh
cat src/frontend/web/react-admin/README.md
```